FROM ubuntu
RUN apt install build-essential libxinerma-dev g++ libx11-dev xinit libx11-dev libxft-dev git &&\
 git clone https://gitlab.com/sharmaganesha2/dwm.git &&\
 cd dwm &&\
 make &&\
 make install &&\
 cd .. &&\
 rm -frv dwm
ENTRYPOINT [ "dwm" ]
